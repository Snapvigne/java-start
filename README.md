## What ?

This is a simple dummy project: just a pom description and a Demo class with
"Hello World"

## How to ?

Build it with maven (mvn install) and just run `java -cp target/java-start-1.0-SNAPSHOT.jar com.codelutin.Demo`
