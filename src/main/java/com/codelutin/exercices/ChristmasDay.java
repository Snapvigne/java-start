package com.codelutin.exercices;

import java.util.Scanner;

 /* CONSIGNES
    Calculer le jour de la semaine où tombe Noël pour une année, entre 2000 et 2099,saisie au clavier, sachant que:
        - en 2000, Noel était un lundi.
        - noel progresse d'un jour chaque année, sauf bissextile où +2
        - 2000 et 2004 sont bissextile
    */


public class ChristmasDay {

    public static void main(String[] args) {
        Scanner myScan = new Scanner(System.in);
        String table[] = {"lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi", "dimanche"};

        System.out.println("Saisissez une année supérieure à 2000.");
        int annee = myScan.nextInt();

        while (annee<2000) {
            System.out.println("On vous a dit SUPERIEURE à 2000!! Recommencez.");
            annee = myScan.nextInt();
        }
            int increment = (annee - 2000 + ((annee - 2000) / 4)) % 7;
            System.out.println("Le jour de Noêl tombe un " + table[increment]);
    }
}
